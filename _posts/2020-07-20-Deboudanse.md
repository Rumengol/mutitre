---
layout: post
title: Deboudanse
date: 2020-07-20 11:48:33 +0200
author: Polyson
---

La deboudanse, c'est l'exercice - ou plutôt l'art - d'être en équilibre instable quand on ne se déplace pas. Favorisé par la consomation contrôlé d'alcool.
